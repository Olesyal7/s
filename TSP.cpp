﻿// TSP.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <algorithm>
#include <iostream>
#include <vector>
#include <cmath>
#include <iterator>
#include <ctime>

using namespace std;

class Point {
public:
	int id;
	float x;
	float y;
	Point(int id_, float x_, float y_) {
		id = id_;
		x = x_;
		y = y_;
	}
	Point() {}
	bool is_equal(Point b) {
		return x == b.x && y == b.y;
	}
};



vector<Point>* read_points(int n) {
	auto points = new vector<Point>();
	int id;
	float x, y;
	for (int i = 0; i < n; i++) {
		cin >> id >> x >> y;
		points->emplace_back(Point(id, x, y));
	}
	return points;
}

float distance(Point a, Point b) {
	if (a.is_equal(b)) {
		return INFINITY;
	}
	return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));
}

class Edge {
public:
	Edge(Point point_1, Point point_2) {
		begin = point_1;
		end = point_2;
		length = distance(point_1, point_2);
	}
	Edge() {}
	bool is_equal(Edge edge) {
		return begin.is_equal(edge.begin) && end.is_equal(edge.end)
			|| begin.is_equal(edge.end) && end.is_equal(edge.begin);
	}
	Point begin;
	Point end;
	float length;
};

void nearest_neighbour(Point current, vector<Point>* remaining_points, vector<Point>* path) {
	auto nearest_neighbour = remaining_points->begin();
	float nearest_distation = INFINITY;
	Point point;
	for (auto i = remaining_points->begin(); i < remaining_points->end(); i++) {
		float current_distation = distance(current, *i);
		if (current_distation < nearest_distation) {
			nearest_distation = current_distation;
			nearest_neighbour = i;
			point = *i;
		}
	}
	remaining_points->erase(nearest_neighbour);
	path->push_back(point);
}

vector<Point>* path(vector<Point> points) {
	int number = points.size();
	auto path = new vector<Point>();
	path->push_back(points[0]);
	points.erase(points.begin());
	for (int i = 0; i < number - 1; ++i) {
		nearest_neighbour(path->back(), &points, path);
	}
	path->push_back(path->front());
	return path;
}


float cycle_length(vector<Point>* point) {
	float sum = 0;
	for (auto i = point->begin(); i < prev(point->end()); ++i) {
		sum += distance(*i, *next(i));
	}
	return sum;
}

float cycle_length(vector<Edge>* edges) {
	float sum = 0;
	for (auto &i : *edges) {
		sum += i.length;
	}
	return sum;
}

vector<Point>* random_first_point(vector<Point>* points) {
	int times = 10;
	int size = points->size();
	auto cycle = path(*points);
	float cycle_len = cycle_length(cycle);
	for (int i = 0; i <= min(size, times); ++i) {
		int k = rand() % points->size();
		swap((*points)[0], (*points)[k]);
		auto new_cycle = path(*points);
		float new_cycle_len = cycle_length(new_cycle);
		if (new_cycle_len < cycle_len) {
			delete(cycle);
			cycle = new_cycle;
			cycle_len = new_cycle_len;
		}
		else {
			delete(new_cycle);
		}
	}
	return cycle;
}

vector<Edge>* convert_to_edge_list(vector<Point>* cycle) {
	auto edge_cycle = new vector<Edge>();
	for (auto i = cycle->begin(); i < prev(cycle->end()); ++i) {
		edge_cycle->emplace_back(Edge(*i, *(i + 1)));
	}
	return edge_cycle;
}

vector<Point>* convert_to_vertex_list(vector<Edge>* edges) {
	auto points = new vector<Point>;
	points->emplace_back(edges->begin()->begin);
	points->emplace_back(edges->begin()->end);
	edges->erase(edges->begin());
	for (int i = 0; i < edges->size(); ++i) {
		for (auto edge = edges->begin(); edge < edges->end(); ++edge) {
			if ((edge->begin).is_equal(points->back())) {
				points->emplace_back(edge->end);
				edges->erase(edge);
				break;
			}
			if ((edge->end).is_equal(points->back())) {
				points->emplace_back(edge->begin);
				edges->erase(edge);
				break;
			}
		}
	}
	points->emplace_back(points->front());
	return points;
}

bool step_of_local_search(vector<Edge>* cycle) {
	float max = 0;
	Edge* swap_i = nullptr;
	Edge* swap_j = nullptr;
	for (auto i = cycle->begin(); i < prev(cycle->end()); ++i) {
		for (auto j = i + 2; j < cycle->end(); ++j) {
			if (i->begin.is_equal(j->end)) {
				break;
			}
			float change = distance(i->begin, i->end) + distance(j->begin, j->end)
				- distance(i->begin, j->begin) - distance(i->end, j->end);
			if (change > max) {
				swap_i = &*i;
				swap_j = &*j;
				max = change;
			}
		}
	}
	if (swap_i && swap_j) {
		swap(swap_j->begin, swap_i->end);
		for (auto i = swap_i + 1; i < swap_j; ++i) {
			swap(i->begin, i->end);
		}
		return true;
	}
	return false;
}


int main() {
	srand(unsigned(time(0)));
	unsigned int n = 0;
	cin >> n;
	auto points = read_points(n);
	auto cycle = random_first_point(points);
	//auto cycle = path(*points);
	auto edge_cycle = convert_to_edge_list(cycle);
	//auto vertex = convert_to_vertex_list(edge_cycle);
	//cout << cycle_length(edge_cycle) << endl;
	/*while (step_of_local_search(edge_cycle)) {
		int a = 0;
	}
	*/
	for (int i = 0; i < 1000; ++i) {
		if (!step_of_local_search(edge_cycle)) {
			break;
		}
	}
	//auto c = convert_to_vertex_list(edge_cycle);
	for (auto i = edge_cycle->begin(); i < edge_cycle->end(); ++i) {
		cout << i->begin.id << " ";
	}
	cout << (edge_cycle->begin()->begin).id << endl;
	cout << cycle_length(edge_cycle) << endl;
	return 0;
}

/*
4
1 0 0
2 1 0
3 -2 2
4 6 -2

4
1 1 0
2 -1 0
3 0 3
4 0 -5

7
1 0 0
2 5 -2
3 2 -3
4 6 8
5 3 1
6 6 -3
7 1 2

4
1 0 0
2 2 2
3 2 -4
4 0 12

8
1 0 0
2 3 3
3 4 -4
4 5 1
5 6 -2
6 8 3
7 10 -1
8 14 0
*/ 