#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <list>
#include <unordered_map>

using namespace std;

class Point3D {
public:
	Point3D() {
		x = 0;
		y = 0;            //Defolt point (0, 0)
		z = 0;
	}
	Point3D(double _x, double _y, double _z, int _num) {
		x = _x;
		y = _y;
		z = _z;
		num = _num;
	}
	double x = 0;
	double y = 0;
	double z = 0;
	int num;
};

class Vector3D {
public:
	Vector3D() {
		x = 0;
		y = 0;
		z = 0;
	}
	Vector3D(Point3D _begin, Point3D _end) {
		x = _end.x - _begin.x;
		y = _end.y - _begin.y;
		z = _end.z - _begin.z;
	}
	Vector3D(double _x, double _y, double _z) {
		x = _x;
		y = _y;
		z = _z;
	}
	double x;
	double y;
	double z;
};

void operator+=(Vector3D& first_vector, const Vector3D& second_vector) {
	first_vector.x += second_vector.x;
	first_vector.y += second_vector.y;
	first_vector.z += second_vector.z;
}
Vector3D operator+(const Vector3D& first_vector, const Vector3D& second_vector) {
	Vector3D result;
	result += first_vector;
	result += second_vector;
	return result;
}

Point3D operator+(const Point3D& point, const Vector3D& vector) {
	Point3D result;
	result.x = point.x + vector.x;
	result.y = point.y + vector.y;
	return result;
}
bool x_less(Point3D& p1, Point3D& p2) {
	return p1.x <= p2.x;
}

bool y_less(Point3D p1, Point3D& p2) {
	return p1.y <= p2.y;
}

void operator-=(Vector3D& first_vector, const Vector3D& second_vector) {
	first_vector.x -= second_vector.x;
	first_vector.y -= second_vector.y;
	first_vector.z -= second_vector.z;
}

Vector3D operator-(const Vector3D& first_vector, const Vector3D& second_vector) {
	Vector3D result;
	result += first_vector;
	result -= second_vector;
	return result;
}

Vector3D operator*(const Vector3D& vector, double scal) {
	Vector3D result;
	result.x = vector.x * scal;
	result.y = vector.y * scal;
	result.z = vector.z * scal;
	return result;
}


double operator*(const Vector3D& first_vector, const Vector3D& second_vector) {
	return first_vector.x * second_vector.x + first_vector.y * second_vector.y + first_vector.z * second_vector.z;
}

Vector3D vector_mult(const Vector3D& first_vector, const Vector3D& second_vector) {
	Vector3D result;
	result.x = first_vector.y * second_vector.z - first_vector.z * second_vector.y;
	result.y = first_vector.x * second_vector.z - second_vector.x * first_vector.z;
	result.z = first_vector.x * second_vector.y - second_vector.x * first_vector.y;
	return result;
}

double mod(const Vector3D& vector) {
	return sqrt(pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2));
}

double mix_mul(const Vector3D first_vector, const Vector3D second_vector, const Vector3D third_vector) {
	return first_vector.x * second_vector.y * third_vector.z + first_vector.z * second_vector.x * third_vector.y
		+ first_vector.y * second_vector.z * third_vector.x - first_vector.z * second_vector.y * third_vector.x
		- first_vector.x * second_vector.z * third_vector.y - first_vector.y * second_vector.x * third_vector.z;
}

Point3D read_point(int num) {
	Point3D result;
	cin >> result.x >> result.y >> result.z;
	result.num = num;
	return result;
}

bool on_segment(Point3D& a, Point3D& b, Point3D& c) {
	double beg_x, end_x, beg_y, end_y;
	beg_x = min(b.x, c.x);
	end_x = max(b.x, c.x);
	beg_y = min(b.y, c.y);
	end_y = min(b.y, c.y);
	if (beg_x <= a.x  && a.x <= end_x && beg_y <= a.y && a.y <= end_y) {
		return true;
	}
	return false;
}

class poligon {
public:
	poligon() {};
	poligon(Point3D p1, Point3D p2, Point3D p3) {
		gr.push_back(p1);
		gr.push_back(p2);
		gr.push_back(p3);
	}
	vector<Point3D> gr;
};

bool is_eye(poligon& pol, Point3D p, Point3D inner) {
	Vector3D v1(pol.gr[0], pol.gr[1]);
	Vector3D v2(pol.gr[1], pol.gr[2]);
	Vector3D v3(pol.gr[1], inner);
	Vector3D v4(pol.gr[1], p);
	if (mix_mul(v1, v2, v3) * mix_mul(v1, v2, v4) < 0) {
		return true;
	}
	return false;
}


struct edge {
	edge(Point3D p1, Point3D p2, poligon pol1, poligon pol2) {
		begin = p1;
		end = p2;
		firstpol = pol1;
		secondpol = pol2;
	}
	Point3D begin;
	Point3D end;
	poligon firstpol;
	poligon secondpol;
};

bool operator<(poligon& p1, poligon& p2) {
	if (p1.gr[0].num < p2.gr[0].num) {
		return true;
	}
	if (p1.gr[0].num == p2.gr[0].num && p1.gr[1].num < p2.gr[1].num) {
		return true;
	}
	if (p1.gr[0].num == p2.gr[0].num && p1.gr[1].num == p2.gr[1].num && p1.gr[2].num < p2.gr[2].num) {
		return true;
	}
	return false;
}

void sortp(pair<Point3D, Point3D>& p) {
	if (p.first.num > p.second.num) {
		swap(p.first, p.second);
	}
}

class solve {
public:
	Point3D inner;
	list<poligon> fig;
	vector<Point3D> points;
	list<edge> edges;
	unordered_map<int, poligon> wait;

	void sortpol(poligon& pol) {
		if (pol.gr[0].num > pol.gr[1].num) {
			swap(pol.gr[0], pol.gr[1]);
		}
		if (pol.gr[1].num > pol.gr[2].num) {
			swap(pol.gr[1], pol.gr[2]);
		}
		if (pol.gr[0].num > pol.gr[1].num) {
			swap(pol.gr[0], pol.gr[1]);
		}
	}

	void sortedge(edge& e) {
		if (e.begin.num < e.end.num) {
			swap(e.begin, e.end);
		}
	}

	solve(vector<Point3D>& arr) {
		points = arr;
		init();
	}

	void init() {
		poligon p012(points[0], points[1], points[2]);
		sortpol(p012);
		poligon p013(points[0], points[1], points[3]);
		sortpol(p013);
		poligon p023(points[0], points[2], points[3]);
		sortpol(p023);
		poligon p123(points[1], points[2], points[3]);
		sortpol(p123);
		fig.push_back(p012);
		fig.push_back(p013);
		fig.push_back(p023);
		fig.push_back(p123);
		edge e01(points[0], points[1], p012, p013);
		sortedge(e01);
		edge e02(points[0], points[2], p012, p023);
		sortedge(e02);
		edge e03(points[0], points[3], p013, p023);
		sortedge(e03);
		edge e12(points[1], points[2], p012, p123);
		sortedge(e12);
		edge e13(points[1], points[3], p013, p123);
		sortedge(e13);
		edge e23(points[2], points[3], p023, p123);
		sortedge(e23);
		edges.push_back(e01);
		edges.push_back(e02);
		edges.push_back(e03);
		edges.push_back(e12);
		edges.push_back(e13);
		edges.push_back(e23);
		Point3D in((points[0].x + points[1].x + points[2].x + points[3].x) / 4, (points[0].y + points[1].y + points[2].y + points[3].y) / 4, (points[0].z + points[1].z + points[2].z + points[3].z) / 4, 0);
		inner = in;
	}

	int pairhasher(pair<Point3D, Point3D> p) {
		return p.first.x + p.first.y * 100 + p.second.x * 1000 + p.second.y * 10000;
	};

	void alg() {
		for (auto i = points.begin() + 4; i != points.end(); i++) {
			addpoint(*i);
		}
		cout << fig.size() << endl;

		for (auto i = fig.begin(); i != fig.end(); i++) {
			endsort(*i);
			//cout << 3 << " " << (i->gr)[0].num << " " << (i->gr)[1].num << " " << (i->gr)[2].num << endl;

		};
		fig.sort();
		for (auto i = fig.begin(); i != fig.end(); i++) {
			//endsort(*i);
			cout << 3 << " " << (i->gr)[0].num << " " << (i->gr)[1].num << " " << (i->gr)[2].num << endl;

		};
	}

	void endsort(poligon& p) {
		Vector3D v1(p.gr[0], p.gr[1]);
		Vector3D v2(p.gr[0], p.gr[2]);
		Vector3D v3(p.gr[0], inner);
		if (mix_mul(v1, v2, v3) > 0) {
			swap(p.gr[1], p.gr[2]);
		}
	}

	void addpoint(Point3D p) {
		for (auto i = fig.begin(); i != fig.end(); i) {
			if (is_eye(*i, p, inner)) {
				i++;
				fig.erase(prev(i));
			}
			else {
				i++;
			}
		}
		auto end = edges.end();
		for (auto i = edges.begin(); i != end; i) {
			if (is_eye(i->firstpol, p, inner) && is_eye(i->secondpol, p, inner)) {
				i++;
				edges.erase(prev(i));
			}
			else if (!is_eye(i->firstpol, p, inner) && is_eye(i->secondpol, p, inner)) {
				poligon pbep(i->begin, i->end, p);
				sortpol(pbep);
				fig.push_back(pbep);
				pair<Point3D, Point3D> pb = make_pair(i->begin, p);
				sortp(pb);
				if (wait.find(pairhasher(pb)) == wait.end()) {
					wait.insert(make_pair(pairhasher(pb), pbep));
				}
				else {
					poligon pol = wait[pairhasher(pb)];
					edge e(p, i->begin, pbep, pol);
					sortedge(e);
					edges.push_back(e);
					wait.erase(pairhasher(pb));
				}
				pair<Point3D, Point3D> pe = make_pair(i->end, p);
				sortp(pe);
				if (wait.find(pairhasher(pe)) == wait.end()) {
					wait.insert(make_pair(pairhasher(pe), pbep));
				}
				else {
					poligon pol = wait[pairhasher(pe)];
					edge e(p, i->end, pbep, pol);
					sortedge(e);
					edges.push_back(e);
					wait.erase(pairhasher(pe));
				}
				edge e1(i->begin, i->end, i->firstpol, pbep);
				sortedge(e1);
				edges.push_back(e1);
				i++;
				edges.erase(prev(i));
			}
			else if (!is_eye(i->secondpol, p, inner) && is_eye(i->firstpol, p, inner)) {
				poligon pbep(i->begin, i->end, p);
				sortpol(pbep);
				fig.push_back(pbep);
				pair<Point3D, Point3D> pb = make_pair(i->begin, p);
				sortp(pb);
				if (wait.find(pairhasher(pb)) == wait.end()) {
					wait.insert(make_pair(pairhasher(pb), pbep));
				}
				else {
					poligon pol = wait[pairhasher(pb)];
					edge e(p, i->begin, pbep, pol);
					sortedge(e);
					edges.push_back(e);
					wait.erase(pairhasher(pb));
				}
				pair<Point3D, Point3D> pe = make_pair(i->end, p);
				sortp(pe);
				if (wait.find(pairhasher(pe)) == wait.end()) {
					wait.insert(make_pair(pairhasher(pe), pbep));
				}
				else {
					poligon pol = wait[pairhasher(pe)];
					edge e(p, i->end, pbep, pol);
					sortedge(e);
					edges.push_back(e);
					wait.erase(pairhasher(pe));
				}
				edge e2(i->begin, i->end, i->secondpol, pbep);
				edges.push_back(e2);
				i++;
				edges.erase(prev(i));
			}
			else {
				i++;
			}
		}
	}
};



int main() {
	int m = 0;
	cin >> m;
	for (int j = 0; j < m; j++) {
		int n = 0;
		cin >> n;
		vector<Point3D> arr;
		Point3D a;
		for (int i = 0; i < n; i++) {
			a = read_point(i);
			arr.push_back(a);
		}
		solve s(arr);
		s.alg();
	}
}

