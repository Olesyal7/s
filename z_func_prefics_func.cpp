#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <set>

using namespace std;

void prefics_function(const string &my_string, vector<int> &pref_funct);

void z_function(const string &my_string, vector<int> &z_funct);

void from_prefics_function_to_string(const vector<int> &prefics_func, string &my_string);

void from_z_function_to_string(const vector<int> &z_funct, string &my_string);

void from_prefics_to_z(const vector<int> &prefics_func, vector<int> &z_func);

void from_z_to_prefics(const vector<int> &z_func, vector<int> &prefics_func);

void prefics_function(const string &my_string, vector<int> &pref_funct) {
    pref_funct.assign(my_string.length(), 0);
    for (int i = 1; i < my_string.length(); i++) {
        int prev = pref_funct[i - 1];
        while (prev > 0 && my_string[i] != my_string[prev]) {
            prev = pref_funct[prev - 1];
        }
        if (my_string[i] == my_string[prev]) {
            prev++;
        }
        pref_funct[i] = prev;
    }
}

void z_function(const string &my_string, vector<int> &z_funct) {
    z_funct.assign(my_string.length(), 0);
    z_funct[0] = my_string.length();
    int left = 0, right = 0;
    for (int i = 1; i < my_string.length(); i++) {
        z_funct[i] = max(0, min(right - i, z_funct[i - left]));
        while (i + z_funct[i] < my_string.length() and my_string[z_funct[i]] == my_string[i + z_funct[i]]) {
            z_funct[i]++;
            if (i + z_funct[i] > right) {
                left = i;
                right = i + z_funct[i];
            }
        }
    }
}

void from_prefics_function_to_string(const vector<int> &prefics_func, string &my_string) {
    char min_char = 'a';
    my_string += min_char;
    int prev_prefics = 0;
    set<char> ban;
    ban.insert(min_char);
    for (int i = 1; i < prefics_func.size(); i++) {
        if (prefics_func[i] == 0) {
            prev_prefics = prefics_func[i - 1];
            ban.insert('a');
            ban.insert(my_string[prev_prefics]);
            while (prev_prefics > 0) {
                ban.insert(my_string[prev_prefics]);
                prev_prefics = prefics_func[prev_prefics - 1];
            }
            min_char = 'a';
            while (ban.find(min_char) != ban.end()) {
                min_char++;
            }
            my_string += min_char;
            ban.clear();
        } else {
            my_string += my_string[prefics_func[i] - 1];
        }
    }
}

void from_z_function_to_string(const vector<int> &z_funct, string &my_string) {
    int len_pref = 0, num_cur_copy = 1;
    set<char> ban;
    vector<int> num_ban;
    char min_char = 'a';
    my_string = 'a';
    ban.insert('a');
    for (int i = 1; i < z_funct.size(); i++) {
        if (z_funct[i] == 0 && len_pref == 0) {
            for (int k = 0; k < num_ban.size(); k++) {
                ban.insert(my_string[num_ban[k]]);
            }
            while (ban.find(min_char) != ban.end()) {
                min_char++;
            }
            my_string += min_char;
            min_char = 'a';
            ban.clear();
            num_ban.clear();
        }
        if (z_funct[i] > len_pref) {
            len_pref = z_funct[i];
            num_cur_copy = 0;
            num_ban.clear();
            num_ban.push_back(i);
        }
        if (z_funct[i] == len_pref) {
            num_ban.push_back(len_pref);
        }
        if (len_pref > 0) {
            my_string += my_string[num_cur_copy];
            num_cur_copy++;
            len_pref--;
        }
    }
}

void from_prefics_to_z(const vector<int> &prefics_func, vector<int> &z_func) {
    z_func.assign(prefics_func.size(), 0);
    for (int i = 1; i < prefics_func.size(); i++) {
        if (prefics_func[i] > 0) {
            z_func[i - prefics_func[i] + 1] = prefics_func[i];
        }
    }
    z_func[0] = z_func.size();
    if (z_func[1] > 0) {
        for (int i = 1; i < z_func[1]; i++) {
            z_func[i + 1] = z_func[1] - i;
        }
    }
    int t;
    for (int i = z_func[1] + 1; i < prefics_func.size() - 1; i++) {
        int j = 0;
        if (z_func[i] > 0 && z_func[i + 1] == 0) {
            j = 1;
            while (j < z_func[i] && z_func[i + j] <= z_func[j]) {
                z_func[i + j] = min(z_func[j], z_func[i] - j);
                j++;
            }
        }
        i = i + j;
    }
}

void from_z_to_prefics(const vector<int> &z_func, vector<int> &prefics_func) {
    prefics_func.assign(z_func.size(), 0);
    for (int i = 1; i < z_func.size(); i++) {
        if (z_func[i] > 0) {
            int j = z_func[i] - 1;
            while (j >= 0 && prefics_func[i + j] == 0) {
                prefics_func[i + j] = j + 1;
                j--;
            }
        }
    }
}

void read(vector<int> &v) {
    int k = 0;
    cin >> k;
    while (k != -1) {
        v.push_back(k);
        cin >> k;
    }
}

void read(string &s) {
    cin >> s;
}

void print(vector<int> &v) {
    for (int i = 0; i < v.size(); i++) {
        cout << v[i] << " ";
    }
    cout << endl;
}

void print(string &s) {
    cout << s << endl;
}


int main() {
    string string_for_prefics, string_for_z;
    read(string_for_prefics);
    read(string_for_z);
    vector<int> vector_for_from_prefics_to_string, vector_for_from_z_to_string;
    read(vector_for_from_prefics_to_string);
    read(vector_for_from_z_to_string);
    vector<int> vector_for_from_prefics_to_z, vector_for_from_z_to_prefics;
    read(vector_for_from_prefics_to_z);
    read(vector_for_from_z_to_prefics);
    vector<int> vector_for_prefics_result;
    vector<int> vector_for_z_result;
    string string_for_from_prefics_to_string_result, string_for_from_z_to_string_result;
    vector<int> vector_for_from_prefics_to_z_result;
    vector<int> vector_for_from_z_to_prefics_result;
    prefics_function(string_for_prefics, vector_for_prefics_result);
    z_function(string_for_z, vector_for_z_result);
    from_prefics_function_to_string(vector_for_from_prefics_to_string, string_for_from_prefics_to_string_result);
    from_z_function_to_string(vector_for_from_z_to_string, string_for_from_z_to_string_result);
    from_prefics_to_z(vector_for_from_prefics_to_z, vector_for_from_prefics_to_z_result);
    from_z_to_prefics(vector_for_from_z_to_prefics, vector_for_from_z_to_prefics_result);
    print(vector_for_prefics_result);
    print(vector_for_z_result);
    print(string_for_from_prefics_to_string_result);
    print(string_for_from_z_to_string_result);
    print(vector_for_from_prefics_to_z_result);
    print(vector_for_from_z_to_prefics_result);
    return 0;
}