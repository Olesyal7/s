#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

const int ALPH_SIZE = 28;
const int FIRST_SYMBOL = 'a' - 2;
const int SECOND_SYMBOL = 'a' - 1;

void first_sort(const string& my_string, vector<int>& permutation, vector<int>& classes, int& num_classes) {
    vector<int> count(ALPH_SIZE, 0);
    for (int i = 0; i < my_string.length(); i++) {
        count[my_string[i] - FIRST_SYMBOL]++;
    }
    for (int i = 1; i < ALPH_SIZE; i++) {
        count[i] += count[i - 1];
    }
    for (int i = my_string.length() - 1; i >= 0; i--) {
        count[my_string[i] - FIRST_SYMBOL]--;
        permutation[count[my_string[i] - FIRST_SYMBOL]] = i;
    }
    classes[permutation[0]] = 0;
    num_classes = 1;
    for (int i = 1; i < my_string.length(); ++i) {
        if (my_string[permutation[i]] != my_string[permutation[i - 1]]) {
            num_classes++;
        }
        classes[permutation[i]] = num_classes - 1;
    }
}

void i_sort(const string& my_string, vector<int>& permutation, vector<int>& classes, int& num_classes, int iteration) {
    vector<int> second_part_permutation(my_string.length(), 0);
    for (int i = 0; i < my_string.length(); i++) {
        second_part_permutation[i] = (my_string.length() + permutation[i] - (1 << iteration)) % my_string.length();
    }
    vector<int> count(num_classes, 0);
    for (int i = 0; i < my_string.length(); i++) {
        count[classes[second_part_permutation[i]]]++;
    }
    for (int i = 1; i < num_classes; i++) {
        count[i] += count[i - 1];
    }
    for (int i = my_string.length() - 1; i >= 0; i--) {
        count[classes[second_part_permutation[i]]]--;
        permutation[count[classes[second_part_permutation[i]]]] = second_part_permutation[i];
    }
    vector<int> new_classes(my_string.length(), 0);
    new_classes[permutation[0]] = 0;
    num_classes = 1;
    for (int i = 1; i < my_string.length(); i++) {
        if (classes[permutation[i]] != classes[permutation[i - 1]] || classes[(permutation[i] + (1 << iteration)) % my_string.length()] !=
                                                                      classes[(permutation[i - 1] + (1 << iteration)) % my_string.length()]) {
            num_classes++;
        }
        new_classes[permutation[i]] = num_classes - 1;
    }
    for (int i = 0; i < my_string.length(); ++i) {
        classes[i] = new_classes[i];
    }
}

void suff_array(const string& s, const string& s2, vector<int>& permutation) {
    string my_string = s + s2;
    vector<int> classes(my_string.length(), 0);
    int num_classes = 0;
    first_sort(my_string, permutation, classes, num_classes);
    for (int iteration = 0; (1 << iteration) < my_string.length(); iteration++) {
        i_sort(my_string, permutation, classes, num_classes, iteration);
    }
}

void Kasai(const string& my_string, const vector<int>& permutation, vector<int>& lcp) {
    vector<int> rank(my_string.length());
    for (int i = 0; i < my_string.length(); i++) {
        rank[permutation[i]] = i;
    }
    int coincidence = 0;
    for (int i = 0; i < my_string.length(); i++) {
        if (coincidence > 0) {
            coincidence--;
        }
        if (rank[i] == my_string.length() - 1) {
            lcp[my_string.length() - 1] = -1;
            coincidence = 0;
        }
        else {
            while (max(i + coincidence, permutation[rank[i] + 1] + coincidence) < my_string.length() && my_string[i + coincidence] ==
                                                                                                        my_string[permutation[rank[i] + 1] + coincidence]) {
                coincidence++;
            }
            lcp[rank[i]] = coincidence;
        }
    }
}

int main()
{
    string my_string_1;
    string my_string_2;
    long long k = 0;
    cin >> my_string_1 >> my_string_2;
    my_string_1 += (char)FIRST_SYMBOL;
    my_string_2 += (char)SECOND_SYMBOL;
    cin >> k;
    vector<int> permutation(my_string_1.length() + my_string_2.length(), 0);
    suff_array(my_string_1, my_string_2, permutation);
    string my_string = my_string_1 + my_string_2;
    vector<int> lcp(my_string.length(), 0);
    Kasai(my_string, permutation, lcp);
    int cur = 0;
    int i = 0;
    long long count = 0;
    string out = "";
    while (i < lcp.size() - 1 && count < k) {
        if ((permutation[i] < my_string_1.length() && permutation[i + 1] >= my_string_1.length()) || (permutation[i + 1] < my_string_1.length() && permutation[i] >= my_string_1.length())) {
            if (lcp[i] > cur) {
                if (count + lcp[i] - cur < k) {
                    count += lcp[i] - cur;
                    cur = lcp[i];
                }
                else {
                    for (int j = 0; j < cur + k - count; j++) {
                        out += my_string[permutation[i] + j];
                    }
                    count = k;
                }
            }
            else {
                cur = lcp[i];
            }
        }
        else {
            if (lcp[i] < cur) {
                cur = lcp[i];
            }
        }
        i++;
    }
    if (out.length() == 0) {
        cout << -1;
        return 0;
    }
    cout << out;
    return 0;
}
