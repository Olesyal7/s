#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

const int ALPH_SIZE = 27;
const int FIRST_SYMBOL = 'a' - 1;

void first_sort(const string& my_string, vector<int>& permutation, vector<int>& classes, int& num_classes) {
    vector<int> count(ALPH_SIZE, 0);
    for (int i = 0; i < my_string.length(); i++) {
        count[my_string[i] - FIRST_SYMBOL]++;
    }
    for (int i = 1; i < ALPH_SIZE; i++) {
        count[i] += count[i - 1];
    }
    for (int i = my_string.length() - 1; i >= 0; i--) {
        count[my_string[i] - FIRST_SYMBOL]--;
        permutation[count[my_string[i] - FIRST_SYMBOL]] = i;
    }
    classes[permutation[0]] = 0;
    num_classes = 1;
    for (int i = 1; i < my_string.length(); ++i) {
        if (my_string[permutation[i]] != my_string[permutation[i - 1]]) {
            num_classes++;
        }
        classes[permutation[i]] = num_classes - 1;
    }
}

void i_sort(const string& my_string, vector<int>& permutation, vector<int>& classes, int& num_classes, int iteration) {
    vector<int> second_part_permutation(my_string.length(), 0);
    for (int i = 0; i < my_string.length(); i++) {
        second_part_permutation[i] = (my_string.length() + permutation[i] - (1 << iteration)) % my_string.length();
    }
    vector<int> count(num_classes, 0);
    for (int i = 0; i < my_string.length(); i++) {
        count[classes[second_part_permutation[i]]]++;
    }
    for (int i = 1; i < num_classes; i++) {
        count[i] += count[i - 1];
    }
    for (int i = my_string.length() - 1; i >= 0; i--) {
        count[classes[second_part_permutation[i]]]--;
        permutation[count[classes[second_part_permutation[i]]]] = second_part_permutation[i];
    }
    vector<int> new_classes(my_string.length(), 0);
    new_classes[permutation[0]] = 0;
    num_classes = 1;
    for (int i = 1; i < my_string.length(); i++) {
        if (classes[permutation[i]] != classes[permutation[i - 1]] || classes[(permutation[i] + (1 << iteration)) % my_string.length()] !=
                                                                      classes[(permutation[i - 1] + (1 << iteration)) % my_string.length()]) {
            num_classes++;
        }
        new_classes[permutation[i]] = num_classes - 1;
    }
    for (int i = 0; i < my_string.length(); ++i) {
        classes[i] = new_classes[i];
    }
}

void suff_array(const string& s, vector<int>& permutation) {
    string my_string = s + (char)FIRST_SYMBOL;
    vector<int> classes(my_string.length(), 0);
    int num_classes = 0;
    first_sort(my_string, permutation, classes, num_classes);
    for (int iteration = 0; (1 << iteration) < my_string.length(); iteration++) {
        i_sort(my_string, permutation, classes, num_classes, iteration);
    }
}

void Kasai(const string& my_string, const vector<int>& permutation, vector<int>& lcp) {
    vector<int> rank(my_string.length());
    for (int i = 0; i < my_string.length(); i++) {
        rank[permutation[i]] = i;
    }
    int coincidence = 0;
    for (int i = 0; i < my_string.length(); i++) {
        if (coincidence > 0) {
            coincidence--;
        }
        if (rank[i] == my_string.length() - 1) {
            lcp[my_string.length() - 1] = -1;
            coincidence = 0;
        }
        else {
            while (max(i + coincidence, permutation[rank[i] + 1] + coincidence) < my_string.length() && my_string[i + coincidence] ==
                                                                                                        my_string[permutation[rank[i] + 1] + coincidence]) {
                coincidence++;
            }
            lcp[rank[i]] = coincidence;
        }
    }
}

int main() {
    string my_string;
    cin >> my_string;
    vector<int> permutation(my_string.length() + 1, 0);
    suff_array(my_string, permutation);
    vector<int> permutation_without_FIRST_SYMBOL(my_string.length(), 0);
    for (int i = 0; i < my_string.length(); i++) {
        permutation_without_FIRST_SYMBOL[i] = permutation[i + 1];
    }
    vector<int> lcp(my_string.length(), 0);
    Kasai(my_string, permutation_without_FIRST_SYMBOL, lcp);
    int sum = 0;
    for (int i = 0; i < my_string.length() - 1; i++) {
        sum += permutation_without_FIRST_SYMBOL.size() - permutation_without_FIRST_SYMBOL[i] - lcp[i];
    }
    sum += permutation_without_FIRST_SYMBOL.size() - permutation_without_FIRST_SYMBOL[permutation_without_FIRST_SYMBOL.size() - 1];
    cout << sum << endl;
    return 0;
}
