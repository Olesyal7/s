#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;
const double eps = 1e-8;

class Point3D {
public:
	Point3D() {
		x = 0;
		y = 0;            //Defolt point (0, 0)
		z = 0;
	}
	Point3D(double _x, double _y, double _z) {
		x = _x;
		y = _y;
		z = _z;
	}
	double x = 0;
	double y = 0;
	double z = 0;
};

class Vector3D {
public:
	Vector3D() {
		x = 0;
		y = 0;
		z = 0;
	}
	Vector3D(Point3D _begin, Point3D _end) {
		x = _end.x - _begin.x;
		y = _end.y - _begin.y;
		z = _end.z - _begin.z;
	}
	Vector3D(double _x, double _y, double _z) {
		x = _x;
		y = _y;
		z = _z;
	}
	double x;
	double y;
	double z;
};

void operator+=(Vector3D& first_vector, const Vector3D& second_vector) {
	first_vector.x += second_vector.x;
	first_vector.y += second_vector.y;
	first_vector.z += second_vector.z;
}
Vector3D operator+(const Vector3D& first_vector, const Vector3D& second_vector) {
	Vector3D result;
	result += first_vector;
	result += second_vector;
	return result;
}

Point3D operator+(const Point3D& point, const Vector3D& vector) {
	Point3D result;
	result.x = point.x + vector.x;
	result.y = point.y + vector.y;
	return result;
}
bool x_less(Point3D& p1, Point3D& p2) {
	return p1.x <= p2.x;
}

bool y_less(Point3D p1, Point3D& p2) {
	return p1.y <= p2.y;
}

void operator-=(Vector3D& first_vector, const Vector3D& second_vector) {
	first_vector.x -= second_vector.x;
	first_vector.y -= second_vector.y;
	first_vector.z -= second_vector.z;
}

Vector3D operator-(const Vector3D& first_vector, const Vector3D& second_vector) {
	Vector3D result;
	result += first_vector;
	result -= second_vector;
	return result;
}

Vector3D operator*(const Vector3D& vector, double scal) {
	Vector3D result;
	result.x = vector.x * scal;
	result.y = vector.y * scal;
	result.z = vector.z * scal;
	return result;
}


double operator*(const Vector3D& first_vector, const Vector3D& second_vector) {
	return first_vector.x * second_vector.x + first_vector.y * second_vector.y + first_vector.z * second_vector.z;
}

Vector3D vector_mult(const Vector3D& first_vector, const Vector3D& second_vector) {
	Vector3D result;
	result.x = first_vector.y * second_vector.z - first_vector.z * second_vector.y;
	result.y = first_vector.x * second_vector.z - second_vector.x * first_vector.z;
	result.z = first_vector.x * second_vector.y - second_vector.x * first_vector.y;
	return result;
}

double mod(const Vector3D& vector) {
	return sqrt(pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2));
}

double mix_mul(const Vector3D first_vector, const Vector3D second_vector, const Vector3D third_vector) {
	return first_vector.x * second_vector.y * third_vector.z + first_vector.z * second_vector.x * third_vector.y
		+ first_vector.y * second_vector.z * third_vector.x - first_vector.z * second_vector.y * third_vector.x
		- first_vector.x * second_vector.z * third_vector.y - first_vector.y * second_vector.x * third_vector.z;
}

Point3D read_point() {
	Point3D result;
	cin >> result.x >> result.y >> result.z;
	return result;
}

bool on_segment(Point3D& a, Point3D& b, Point3D& c) {
	double beg_x, end_x, beg_y, end_y;
	beg_x = min(b.x, c.x);
	end_x = max(b.x, c.x);
	beg_y = min(b.y, c.y);
	end_y = min(b.y, c.y);
	if (beg_x <= a.x  && a.x <= end_x && beg_y <= a.y && a.y <= end_y) {
		return true;
	}
	return false;
}

int main() {
	Point3D first_beg = read_point();
	Point3D first_end = read_point();
	Point3D second_beg = read_point();
	Point3D second_end = read_point();
	Vector3D first_direct(first_end, first_beg);
	Vector3D second_direct(second_end, second_beg);
	Vector3D connect(first_beg, second_beg);
	double first_direct_2 = first_direct * first_direct;
	double first_dir_second_dir = first_direct * second_direct;
	double second_dir_2 = second_direct * second_direct;
	double first_dir_connect = first_direct * connect;
	double second_dir_connect = second_direct * connect;
	double denominator = first_direct_2 * second_dir_2 - first_dir_second_dir * first_dir_second_dir;
	double first_scal_num = 0;
	double first_scal_den = denominator;
	double second_scal_num = first_dir_connect;
	double second_scal_den = denominator;
	if (denominator < eps) {
		first_scal_den = 1;
		second_scal_num = second_dir_connect;
		second_scal_den = second_dir_2;
	}
	else {
		first_scal_num = (first_dir_second_dir * second_dir_connect - second_dir_2 * first_dir_connect);
		second_scal_num = (first_direct_2 * second_dir_connect - first_dir_second_dir * first_dir_connect);
		if (first_scal_num < eps) {
			first_scal_num = 0;
			second_scal_num = second_dir_connect;
			second_scal_den = second_dir_2;
		}
		else if (first_scal_num - first_scal_den > -eps) {
			first_scal_num = first_scal_den;
			second_scal_num = (second_dir_connect + first_dir_second_dir);
			second_scal_den = second_dir_2;
		}
	}
	if (second_scal_num < eps) {
		second_scal_num = 0;
		if (first_dir_connect > -eps) {
			first_scal_num = 0;
		}
		else if (first_direct_2 + first_dir_connect < eps) {
			first_scal_num = first_scal_den;
		}
		else {
			first_scal_num = -first_dir_connect;
			first_scal_den = first_direct_2;
		}
	}
	else if (second_scal_num - second_scal_den > -eps) {
		second_scal_num = second_scal_den;
		if (first_dir_second_dir - first_dir_connect < eps) {
			first_scal_num = 0;
		}
		else if (first_dir_second_dir - first_direct_2 - first_dir_connect > -eps) {
			first_scal_num = first_scal_den;
		}
		else {
			first_scal_num = (first_dir_second_dir - first_dir_connect);
			first_scal_den = first_direct_2;
		}
	}
	double first_scal = 0;
	double second_scal = 0;
	if (fabs(first_scal_den) < eps) {
		first_scal = 0;
	}
	else {
		first_scal = first_scal_num / first_scal_den;
	}
	if (fabs(second_scal_den) < eps) {
		second_scal = 0;
	}
	else {
		second_scal = second_scal_num / second_scal_den;
	}
	Vector3D res(connect + (first_direct * first_scal) - (second_direct * second_scal));
	cout.setf(ios::fixed);
	cout.precision(8);
	cout << mod(res);
	return 0;
}

