#include <iostream>
#include <vector>
#include <string>

using namespace std;

const int alph_size = 26;
const int alph_beg = 97;
const int nf = -1;
const int root = 0;

struct Node {
    vector<int> son = vector<int>(alph_size, nf);
    vector<int> trans = vector<int>(alph_size, nf); //transition
    int suffics_link = nf;
    int up_suffics_link = nf;
    int parent = nf;
    vector<int> num_pat; //number of puttern
    int alpha;
    bool is_term = false;
};

class corasick {

private:

    int get_sl(int node);

public:
    vector<Node> trie;

    corasick();

    void add_word(const string s, int num_pat);

    int get_trans(int node, int num_alpha);

    int get_usl(int node);


};

corasick::corasick() {
    Node n;
    trie.push_back(n);
}

int corasick::get_sl(int node) {
    if (trie[node].suffics_link == nf) {
        if (node == root || trie[node].parent == root) {
            trie[node].suffics_link = root;
        }
        else {
            trie[node].suffics_link = get_trans(get_sl(trie[node].parent), trie[node].alpha - alph_beg);
        }
    }
    return trie[node].suffics_link;
}

void corasick::add_word(const string s, int num_pat) {
    int cur = root; //current node
    for (int i = 0; i < s.length(); i++) {
        int num_alpha = s[i] - alph_beg;
        if (trie[cur].son[num_alpha] != nf) {
            cur = trie[cur].son[num_alpha];
        }
        else {
            Node n;
            n.alpha = s[i];
            n.parent = cur;
            trie.push_back(n);
            trie[cur].son[num_alpha] = trie.size() - 1;
            cur = trie.size() - 1;
        }
    }
    trie[cur].is_term = true;
    trie[cur].num_pat.push_back(num_pat);
}

int corasick::get_trans(int node, int num_alpha ) {
    if (trie[node].trans[num_alpha] == nf) {
        if (trie[node].son[num_alpha] != nf) {
            trie[node].trans[num_alpha] = trie[node].son[num_alpha];
        }
        else if (node == root) {
            trie[node].trans[num_alpha] = root;
        }
        else
            trie[node].trans[num_alpha] = get_trans(get_sl(node), num_alpha);
    }
    return trie[node].trans[num_alpha];
}

int corasick::get_usl(int node) {
    if (trie[node].up_suffics_link == nf) {
        if (trie[get_sl(node)].is_term) {
            trie[node].up_suffics_link = get_sl(node);
        }
        else if (get_sl(node) == root) {
            trie[node].up_suffics_link = root;
        }
        else {
            trie[node].up_suffics_link = get_usl(get_sl(node));
        }
    }
    return trie[node].up_suffics_link;
}

void search_pattern(const string &s, vector<int> &out, const string pattern) {
    vector<string> patterns;
    string c_pat = ""; //current pattern
    vector<int> start;   // the starting number of a piece of the pattern in the pattern
    for (int i = 0; i < pattern.length(); i++) {
        if (c_pat != "" && pattern[i] == '?') {
            patterns.push_back(c_pat);
            start.push_back(i - c_pat.size());
            c_pat = "";
        }
        else if (pattern[i] != '?') {
            c_pat += pattern[i];
        }
    }
    if (c_pat != "") {
        patterns.push_back(c_pat);
        start.push_back(pattern.length() - c_pat.size());
    }
    vector<int> count(s.length(), 0);
    corasick Corasick;
    for (int i = 0; i < patterns.size(); i++) {
        Corasick.add_word(patterns[i], i);
    }
    int cur = root;
    int cur_up;
    for (int i = 0; i < s.length(); i++) {
        cur = Corasick.get_trans(cur, s[i] - alph_beg);
        cur_up = cur;
        while (cur_up > 0) {
            if (Corasick.trie[cur_up].is_term) {
                for (int j = 0; j < Corasick.trie[cur_up].num_pat.size(); j++) {
                    int starting_number = i + 1 - patterns[Corasick.trie[cur_up].num_pat[j]].length() -
                                          start[Corasick.trie[cur_up].num_pat[j]];
                    if (starting_number >= 0) {
                        count[starting_number]++;
                    }
                }
            }
            cur_up = Corasick.get_usl(cur_up);
        }
    }
    for (int i = 0; i < s.length() - pattern.length() + 1; i++) {
        if (count[i] == patterns.size()) {
            out.push_back(i);
        }
    }
}

int main() {
    string my_string;
    string pattern;
    cin >> pattern;
    cin >> my_string;
    vector<int> out;
    if (my_string.length() >= pattern.length()) {
        search_pattern(my_string, out, pattern);
    }
    for (int i = 0; i < out.size(); i++) {
        cout << out[i] << " ";
    }
    return 0;
}
